---
title: Digitale Bilddaten und Medizintechnik Ursprung - Teil 1
subtitle: Warum Vernetzung wichtig ist
date: 2021-05-07
tags: ["DICOM", "Medizintechnik", "Bilddaten"]
---

Es war einmal eine Medizintechnikanlage, welche in der Lage war, Bilder aufzunehmen und digital zu speichern. Jedoch wurden die Bilder in einem proprietären Format gespeichert, sodass die Bilder nur an dieser einen Medizintechnikanlage betrachtet werden konnten. Diese Anlage war sehr modern und konnte viele zur Diagnose relevante Fragestellungen beantworten. Daher wollte der Gesundheitsdienstleister sie unbedingt haben und hatte sie teuer gekauft, um so seinen Patienten eine möglichst genaue Diagnose zu ermöglichen. Er hatte zusätzlich einen Wartungsvertrag mit dem Medizintechnikanlagen-Hersteller abgeschlossen, um so einen dauerhaften Betrieb sicherzustellen.

Der Gesundheitsdienstleister fragte sich eines Tages, warum kann ich die Bilder nicht auf meinem Arbeitsplatz anschauen sondern muss immer zu der Medizintechnikanlage gehen?

Er kam auf den Gedanken, dass es einfacher gehen musste und schaute nach einer Software, welche die proprietären Bilder auf seinem Arbeitsplatz darstellen konnte.

Er stellt fest, dass nur die Hersteller der Medizintechnikanlage eine Software angeboten hat, welche eine Betrachtung der proprietären Bilder ermöglichte. Jedoch musste man dazu die Bilder von der Medizintechnikanlage erst manuell exportieren auf eine CD. Von dieser CD konnte die Software dann die Bilddaten darstellen auf dem Arbeitsplatz des Gesundheitsdienstleisters.

Um diese Möglichkeit nutzen zu können hat der Gesundheitsdienstleister wieder für teures Geld die Software für den Arbeitsplatz gekauft und das Modul der Medizintechnikanlage, welches ein exportieren der Bilddaten ermöglichte. Zusätzlich wurden die Kosten bei den Wartungsvertrag erhöht, dass nun auch das exportieren immer reibungslos funktionieren soll.

So konnte der Gesundheitsdiensleister nach jeder Untersuchung die Bilddaten nun exportieren auf eine CD und anschließend die Bilddaten auf seinen Arbeitsrechner betrachten. Jedoch musste er nun regelmäßig CDs einkaufen und viel Zeit mit den exportieren und einlesen der Bilddaten verbringen sowie mit dem beschriften der CDs

Also holte er sich eine Person welche ihm die Arbeit für das exportieren, einlesen und beschriften abgenohmen hat, sodass der Gesundheitsdiensleister wieder mehr Zeit für die Diagnose der Patienten hatte.

Er kommt nun jedoch erneut auf den Gedanken, dass es einfacher gehen muss und fragt beim Hersteller ob er nicht eine Netzwerkverbindung zwischen der Medizintechnikanlage und der Software auf seinen Arbeitsrechner einrichten kann, sodass die Bilddaten direkt von der Medizintechnikanlage zu der Software übertragen werden können.

Der Hersteller sagt, dass es eine sehr gute Idee ist. Wir stellen Ihnen eine Lösung vor damit Sie nicht mehr die Person benötigen zum Exportieren und Einlesen der Bilddaten und sie zusätzlich auch keine CDs mehr benötigen jedoch ist die Lösung nicht ganz billig. 

Also sagt sich der Gesundheitsdienstleister das klingt gut, ich werde diese Lösung kaufen und werde viel Zeit und Geld sparen.

So konnte der Gesundheitsdienstleister nach jeder Untersuchung die Bilddaten nun direkt nach einer Untersuchung von der Medizintechnikanlage zu der Software auf seinen Arbeitsrechner übertragen. Er stellte jedoch fest, dass er die Bilddaten immer auf seinen Arbeitsrechner immer eindeutig benennen musste und nun auch mehr Speicher auf seinen Arbeitsrechner benötigte da nun alle Bilddaten auf seinen Arbeitsrechner aufbewahrt wurden und nicht mehr auf den CDs.

Also holte er sich eine Person welche ihm die Arbeit für das Beschriften der Bilddaten abnimmt und schloss einen Wartungsvertrag mit seinen Arbeitsrechner Hersteller ab, dass dieser immer genug Speicher auf dem Arbeitsrechner des Gesundheitsdienstleisters sicherstellt.

Hier ist die Geschichte damit noch nicht zu Ende. Eine Fortsetzung folgt.
