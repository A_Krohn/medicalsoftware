---
title: Erster Eintrag
subtitle: Medizintechnik im Gesundheitswesen
date: 2021-05-06
tags: ["Herausforderungen", "Medizintechnik", "Software"]
---

Wie kommen die Informationen von der Medizintechnik zum Patienten bei einer Behandlung und wo liegt die Schwierigkeit?


> Patient-->Gesundheitsdienstleister;
>
> Gesundheitsdienstleister-->Medizintechnik;
>
> Medizintechnik-->Software;
>
> Software-->Gesundheitsdienstleister;
>
> Gesundheitsdienstleister-->Patient;

Dies ist ein ständiger Kreislauf. 
Ein Patient nimmt einen Gesundheitsdienstleister (Arzt, Hebamme, Pflegekraft usw.) in Anspruch. Anschließend liefert die Medizintechnik über Software eine Rückmeldung an dem Gesundheitsdienstleister als Untersuchungsresultat, welches der Gesundheitsdienstleister dann zusammen mit den Patienten bewerten kann um eine fundierte Diagnose erstellen zu können. Die Diagnose ist dann die Grundlage für eine Behandlung welche das Ziel hat einen stabilen Gesundheitszustand wieder herzustellen.
Kommt der Gesundheitszustand wieder aus dem Gleichgewicht kann wieder ein Gesundheitsdienstleister in Anspruch genommen werden.

Ganz einfach und simpel. Die Schwierigkeit liegt im Detail und zwar wie kommen die Untersuchungsinformationen von der Medizintechnik zum Gesundheitsdienstleister und zum Patienten. Hier ist noch viel Arbeit zu tun und wird es immer viel Arbeit geben um den Informationsfluss immer angenehmer zu gestalten. 

Ein guter Informationfluss liegt vor, wenn dieser einfach und transparent bleibt. Hier kommen Standards ins Spiel welche das ermöglichen. In diesem Blog möchte ich gerne das Augenmerk auf diese Standards legen und wie Informationen fließen sollten anhand von Beispielen.

Ich freue mich Sie auf die Entdeckungsreise in der Medizininformatik zu nehmen.

AKr
